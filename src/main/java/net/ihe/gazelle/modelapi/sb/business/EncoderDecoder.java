package net.ihe.gazelle.modelapi.sb.business;

import net.ihe.gazelle.modelapi.messaging.business.AbstractMessagePartInstance;
import net.ihe.gazelle.modelapi.validationreportmodel.business.ValidationReport;

/**
 * Decoder Encoder API of a standard block.
 *
 * @param <M> concretion of {@link AbstractMessagePartInstance} corresponding to encoded message.
 * @param <C> concretion of {@link AbstractMessagePartInstance} corresponding to decoded message.
 */
public interface EncoderDecoder<M extends AbstractMessagePartInstance, C extends AbstractMessagePartInstance> {

    /**
     * Encode {@link C} content
     *
     * @param content content to encode
     * @return the {@link M} encoded message
     * @throws EncodingException if the encoding cannot be done.
     */
    M encode(C content) throws EncodingException;

    /**
     * Decode and validate {@link M} message
     *
     * @param message message to decode
     * @return the {@link C} decoded content
     * @throws DecodingException if the decoding cannot be done.
     * @throws ValidationException if the encoded content is not valid.
     */
    C decodeAndValidate(M message) throws DecodingException, ValidationException;

    /**
     * Decode and report validation of {@link M} message
     *
     * @param message message to decode
     * @param report report of the validation
     * @return the {@link C} decoded content
     * @throws DecodingException if the decoding cannot be done.
     * @throws ValidationException if the encoded content is not valid.
     */
    C decodeAndReportValidation(M message, ValidationReport report) throws DecodingException, ValidationException;

}
